const winston = require('winston');
const config = require('config');

module.exports = function () {
    if (!config.get('jwtPrivateKey')) {
        throw new Error('Fatal error: jwtPrivateKey is not defined');
    }
    winston.info('Application Name: ' + config.get('name'));
    winston.info('Mail Server: ' + config.get('mail.host'));
};