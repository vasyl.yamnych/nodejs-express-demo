const startupDebugger = require('debug')('app:startup');
// const dbDebugger = require('debug')('app:db');
const morgan = require('morgan');
const winston = require('winston');
// require('winston-mongodb');
require('express-async-errors');

module.exports = function (app) {
    process.on('unhandledRejection', (ex) => {
        throw ex;
    });

    winston.exceptions.handle(new winston.transports.File({filename: 'uncaughtExceptions.json'}));


    winston.add(new winston.transports.Console);
    winston.add(new winston.transports.File({filename: 'logfile.log'}));
//    winston.add(new winston.transports.MongoDB({db: 'mongodb://root:example@localhost:27017/playground?authSource=admin'}));

    if (app.get('env') === 'development') {
        app.use(morgan('tiny'));
        startupDebugger('Morgan enabled...'); // console.log
    }
};